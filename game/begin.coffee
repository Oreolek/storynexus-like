salet.game_id = "3941006b-e592-49fe-8226-c4956cc65e4d"
salet.game_version = "1.0"
# This function is called after entering any new room, so almost every turn.
# I hijack it to update the inventory block.
salet.afterEnter = () ->
  content = ""
  salet.character.qualities ?= []
  for name, value of salet.character.qualities
    if typeof value != 'function'
      content += "* #{name}: #{value}\n"
  $("#inventory").html(marked(content))

$(document).ready(() ->
  salet.beginGame()
)

# The first room of the game.
room "start",
  enter: () ->
    salet.character.setQuality = (name, value) ->
      salet.character.qualities[name] = value
  dsc: """
    #### Welcome to Generic World 101.
    
    It is warm here. You are about to board the train. Who are you?
  """,
  choices: "#start"

choice = (name, spec) ->
  spec.optionText ?= """
    <img src="img/#{spec.img}">
    <div class="text">
      <div class="title">#{spec.title}</div>
      <div class="subtitle">#{spec.subtitle}</div>
    </div>
  """
  return room(name, spec)

choice "f1",
  img: "card-ace-spades.png"
  title: "A bug"
  subtitle: "More legs, more fun."
  after: () ->
    salet.goTo("quality")
  dsc: ""
  tags: ["start"]

choice "f2",
  tags: ["start"]
  img: "card-queen-spades.png"
  after: () ->
    salet.goTo("quality")
  title: "A fish!"
  subtitle: "Tails beat legs. It's a basic Tails-Legs-Tails strategy."
  dsc: ""

room "quality",
  enter: () ->
    salet.character.setQuality("Curiousities", "none")
  ways: ["hotel", "snow"]
  dsc: """
    And now you're on your own. See the waypoints block? Go to the hotel.
  """

room "hotel",
  ways: ["snow"]
  title: "Fancy Hotel"
  dsc: """
    You look at the sign.

    ### Fancy Hotel
    
    It's a middle of nowhere. No hotel in sight. Sigh.
  """

room "snow",
  title: "Snow-covered lands"
  ways: ["hotel"]
  dsc: """
    ### Snow-covered lands
    
    It's so cold here, you suspect it's the North Pole. The snow desert is endless.
  """
